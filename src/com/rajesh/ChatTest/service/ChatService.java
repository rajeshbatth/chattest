package com.rajesh.ChatTest.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import com.rajesh.ChatTest.ChatApp;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.activity.ChatActivity;
import com.rajesh.ChatTest.chat.ChatClient;
import com.rajesh.ChatTest.chat.ChatLoginHandler;
import com.rajesh.ChatTest.dao.Chatter;
import com.rajesh.ChatTest.dao.ChatterDao;
import com.rajesh.ChatTest.dao.DaoSession;
import com.rajesh.ChatTest.misc.ActivityStateCheck;
import com.rajesh.ChatTest.misc.Logger;
import com.rajesh.ChatTest.misc.Session;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by Rajesh on 9/9/2014.
 */
public class ChatService extends Service implements ChatManagerListener, MessageListener {

  public static final String ACTION_LOGIN = "action_login";
  private ChatClient mBinder;
  private DaoSession mDaoSession;

  public static void onBootUp(Context context) {
    if (Session.getUser(context) != null) {
      Intent intent = new Intent(context, ChatService.class);
      intent.setAction(ACTION_LOGIN);
      context.startService(intent);
    }
  }

  @Override
  public void onCreate() {
    super.onCreate();
    mDaoSession = ((ChatApp) getApplication()).getDaoSession();
    mBinder = new ChatClient();
    mBinder.setBackgroundChatListener(this);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    if (intent != null && intent.getAction() != null && intent.getAction().equals(ACTION_LOGIN)) {
      ChatLoginHandler chatLoginHandler = new MyChatLoginHandler();
      mBinder.loginAsync(chatLoginHandler);
    }
    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    mBinder.destroy();
    mBinder = null;
  }

  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  @Override
  public void chatCreated(Chat chat, boolean b) {
    Logger.d("new chat from " + chat.getParticipant());
    chat.addMessageListener(this);
  }

  @Override
  public void processMessage(Chat chat, Message message) {
    String from = message.getFrom().replace("/Smack", "");
    Chatter chatter = mDaoSession.getChatterDao()
        .queryBuilder()
        .where(ChatterDao.Properties.Name.eq(from))
        .unique();
    saveMessage(message, chatter);
    if (!ActivityStateCheck.getInstance().isActive()) {
      createNotification(message, chatter);
    }
  }

  private void createNotification(Message message, Chatter chatter) {
    Intent chatIntent = ChatActivity.getChatIntent(this, chatter);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(this, 0, chatIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    Notification notification =
        new NotificationCompat.Builder(this).setContentTitle(message.getFrom())
            .setContentText(message.getBody())
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_launcher)
            .setAutoCancel(true)
            .build();
    NotificationManager notificationManager =
        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    notificationManager.notify(1, notification);
  }

  private void saveMessage(Message message, Chatter chatter) {
    ChatActivity.saveMessage(message, mDaoSession.getChatMessageDao(), chatter);
  }

  private class MyChatLoginHandler extends ChatLoginHandler {

    public MyChatLoginHandler() {
      super(Session.getUser(ChatService.this));
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onSuccess() {
      Logger.d("logged in as " + getAppUser().getName());
      Session.saveUser(ChatService.this, getAppUser());
    }

    @Override
    public void onFailure() {

    }
  }
}
