package com.rajesh.ChatTest;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import com.rajesh.ChatTest.dao.DaoMaster;
import com.rajesh.ChatTest.dao.DaoSession;

/**
 * Created by Rajesh on 9/12/2014.
 */
public class ChatApp extends Application {

  private DaoSession mDaoSession;
  private DaoMaster mDaoMaster;

  @Override
  public void onCreate() {
    super.onCreate();
    DaoMaster.OpenHelper openHelper = new MyOpenHelper();
    SQLiteDatabase database = openHelper.getWritableDatabase();
    mDaoMaster = new DaoMaster(database);
    mDaoSession = mDaoMaster.newSession();
  }

  public DaoSession getDaoSession() {
    return mDaoSession;
  }

  public DaoMaster getDaoMaster() {
    return mDaoMaster;
  }

  private class MyOpenHelper extends DaoMaster.OpenHelper {

    public MyOpenHelper() {
      super(ChatApp.this, "chat_app", null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }
  }
}
