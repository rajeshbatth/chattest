package com.rajesh.ChatTest.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rajesh on 9/9/2014.
 */
public class AppUser implements Parcelable {
  private String name;
  private String password;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.name);
    dest.writeString(this.password);
  }

  public AppUser() {
  }

  private AppUser(Parcel in) {
    this.name = in.readString();
    this.password = in.readString();
  }

  public static final Parcelable.Creator<AppUser> CREATOR = new Parcelable.Creator<AppUser>() {
    public AppUser createFromParcel(Parcel source) {
      return new AppUser(source);
    }

    public AppUser[] newArray(int size) {
      return new AppUser[size];
    }
  };
}
