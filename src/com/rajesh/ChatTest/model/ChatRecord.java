package com.rajesh.ChatTest.model;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class ChatRecord {

  private List<Message> mMessageList = new ArrayList<Message>();

  private String mChateeName;

  public String getChateeName() {
    return mChateeName;
  }

  public void setChateeName(String chateeName) {
    this.mChateeName = chateeName;
  }

  public List<Message> getMessageList() {
    return mMessageList;
  }

  public void setMessageList(List<Message> messageList) {
    mMessageList = messageList;
  }

  public void addMessage(Message message) {
    mMessageList.add(message);
  }
}
