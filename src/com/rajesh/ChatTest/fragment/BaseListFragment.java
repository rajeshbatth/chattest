package com.rajesh.ChatTest.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import butterknife.ButterKnife;
import com.rajesh.ChatTest.activity.BaseActivity;
import com.rajesh.ChatTest.chat.BaseComponent;
import com.rajesh.ChatTest.chat.BaseComponentImpl;
import com.rajesh.ChatTest.chat.ChatClient;
import com.rajesh.ChatTest.dao.DaoSession;
import com.rajesh.ChatTest.misc.Logger;

/**
 * Created by Rajesh on 9/9/2014.
 */
public abstract class BaseListFragment extends ListFragment implements BaseComponent {

  private BaseActivity mBaseActivity;
  private BaseComponent mBaseComponent;

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    if (!(getActivity() instanceof BaseActivity)) {
      throw new IllegalStateException(
          "Can't add this fragment to activity. Make sure your activity extends BaseActivity");
    }
    mBaseActivity = ((BaseActivity) getActivity());
    mBaseComponent = new BaseComponentImpl(mBaseActivity, this);
  }

  public DaoSession getDaoSession() {
    return mBaseActivity.getDaoSession();
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    ButterKnife.inject(view);
  }

  @Override
  public void onStop() {
    super.onStop();
    unbindChatService();
  }

  @Override
  public void bindChatService() {
    Logger.d("binding service from " + getActivity().getTitle());
    mBaseComponent.bindChatService();
  }

  @Override
  public void unbindChatService() {
    Logger.d("unbinding service from " + getActivity().getTitle());
    mBaseComponent.unbindChatService();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    mBaseComponent.onOptionsItemSelected(item);
    return true;
  }

  public void logout() {
    mBaseComponent.logout();
  }

  @Override
  public void onChatServiceConnected() {
  }

  @Override
  public void onChatServiceDisconnected() {
  }

  @Override
  public ChatClient getChatClient() {
    return mBaseComponent.getChatClient();
  }
}
