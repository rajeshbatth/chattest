package com.rajesh.ChatTest.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.activity.ChatActivity;
import com.rajesh.ChatTest.adapter.RosterAdapter;
import com.rajesh.ChatTest.dao.Chatter;
import com.rajesh.ChatTest.dao.ChatterDao;
import com.rajesh.ChatTest.dao.Marshaller;
import com.rajesh.ChatTest.misc.Logger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;

import static com.rajesh.ChatTest.dao.ChatterDao.Properties.Name;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class RosterListFragment extends BaseListFragment
    implements ChatManagerListener, MessageListener {

  private List<Chatter> mChatterList = new ArrayList<Chatter>();
  private RosterAdapter mRosterAdapter;
  private ChatterDao mChatterDao;

  @Override
  public void onStart() {
    super.onStart();
    bindChatService();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    mRosterAdapter = new RosterAdapter(mChatterList);
    setListAdapter(mRosterAdapter);
    View view = super.onCreateView(inflater, container, savedInstanceState);
    setHasOptionsMenu(true);
    return view;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.base_menu, menu);
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    ChatActivity.startChatActivity(getActivity(), mChatterList.get(position));
  }

  @Override
  public void onChatServiceConnected() {
    mChatterDao = getDaoSession().getChatterDao();
    loadRoster();
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    setEmptyText("You're all alone now :(");
  }

  private void loadRoster() {
    new AsyncTask<Void, Void, Collection<RosterEntry>>() {

      @Override
      protected Collection<RosterEntry> doInBackground(Void... voids) {
        Collection<RosterEntry> rosterEntries = getChatClient().getRosterEntries();
        mChatterList.clear();
        for (RosterEntry rosterEntry : rosterEntries) {
          Chatter chatterTemp =
              mChatterDao.queryBuilder().where(Name.eq(rosterEntry.getUser())).build().unique();
          if (chatterTemp != null) {
            mChatterList.add(chatterTemp);
            continue;
          }
          Chatter chatter = Marshaller.marshallChatter(rosterEntry);
          long id = mChatterDao.insert(chatter);
          chatter.setId(id);
          mChatterList.add(chatter);
        }
        return rosterEntries;
      }

      @Override
      protected void onPostExecute(Collection<RosterEntry> rosterEntries) {
        setListShown(true);
        mRosterAdapter.notifyDataSetChanged();
      }
    }.execute();
  }

  @Override
  public void chatCreated(Chat chat, boolean b) {
    Logger.d("New chat  " + chat.getParticipant());
    chat.addMessageListener(this);
  }

  @Override
  public void processMessage(Chat chat, Message message) {
    Logger.d("new message from " + message.getFrom() + ", message = " + message.getBody());
  }
}
