package com.rajesh.ChatTest.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import org.jivesoftware.smack.RosterEntry;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class RosterAdapter2 extends BaseAdapter {

  private List<RosterEntry> mRosterEntries;

  public RosterAdapter2(List<RosterEntry> rosterEntries) {
    mRosterEntries = rosterEntries;
  }

  @Override
  public int getCount() {
    return mRosterEntries.size();
  }

  @Override
  public Object getItem(int i) {
    return mRosterEntries.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    if (view == null) {
      view = View.inflate(viewGroup.getContext(), android.R.layout.simple_list_item_1, null);
    }
    RosterEntry rosterEntry = mRosterEntries.get(i);
    ((TextView) view).setText(rosterEntry.getName());
    return view;
  }
}
