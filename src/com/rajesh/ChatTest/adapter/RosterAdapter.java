package com.rajesh.ChatTest.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.rajesh.ChatTest.dao.Chatter;
import java.util.List;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class RosterAdapter extends BaseAdapter {

  private List<Chatter> mChatters;

  public RosterAdapter(List<Chatter> chatters) {
    mChatters = chatters;
  }

  @Override
  public int getCount() {
    return mChatters.size();
  }

  @Override
  public Object getItem(int i) {
    return mChatters.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    if (view == null) {
      view = View.inflate(viewGroup.getContext(), android.R.layout.simple_list_item_1, null);
    }
    Chatter rosterEntry = mChatters.get(i);
    ((TextView) view).setText(rosterEntry.getName());
    return view;
  }
}
