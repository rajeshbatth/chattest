package com.rajesh.ChatTest.adapter;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.rajesh.ChatTest.dao.ChatMessage;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class ChatRecordAdapter extends BaseAdapter {

  private final Long mChatterId;
  private List<ChatMessage> mChatMessages = new ArrayList<ChatMessage>();
  private Activity mActivity;

  public ChatRecordAdapter(Activity activity, Long id) {
    mActivity = activity;
    mChatterId = id;
  }

  public Long getChatterId() {
    return mChatterId;
  }

  public synchronized List<ChatMessage> getChatMessages() {
    return mChatMessages;
  }

  public synchronized void setChatMessages(List<ChatMessage> chatMessages) {
    mChatMessages.clear();
    if (chatMessages != null) {
      mChatMessages.addAll(chatMessages);
    }
    postNotifyDataSetChanged();
  }

  public void postNotifyDataSetChanged() {
    mActivity.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        notifyDataSetChanged();
      }
    });
  }

  @Override
  public int getCount() {
    return mChatMessages.size();
  }

  @Override
  public Object getItem(int i) {
    return null;
  }

  @Override
  public long getItemId(int i) {
    return 0;
  }

  @Override
  public View getView(int i, View view, ViewGroup cont) {
    ChatMessage message = mChatMessages.get(i);
    if (view == null) {
      view = View.inflate(cont.getContext(), android.R.layout.simple_list_item_1, null);
    }
    TextView textView = (TextView) view;
    textView.setText(message.getBody());
    if (message.getIsTx()) {
      textView.setGravity(Gravity.LEFT);
    } else {
      textView.setGravity(Gravity.RIGHT);
    }
    return view;
  }
}