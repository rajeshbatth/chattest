package com.rajesh.ChatTest.adapter;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.rajesh.ChatTest.model.ChatRecord;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class ChatRecordAdapterDao extends BaseAdapter {

  private ChatRecord mChatRecord;

  public ChatRecordAdapterDao(ChatRecord chatRecord) {
    mChatRecord = chatRecord;
  }

  @Override
  public int getCount() {
    return mChatRecord.getMessageList().size();
  }

  @Override
  public Object getItem(int i) {
    return null;
  }

  @Override
  public long getItemId(int i) {
    return 0;
  }

  @Override
  public View getView(int i, View view, ViewGroup cont) {
    Message message = mChatRecord.getMessageList().get(i);
    if (view == null) {
      view = View.inflate(cont.getContext(), android.R.layout.simple_list_item_1, null);
    }
    TextView textView = (TextView) view;
    textView.setText(message.getBody());
    if (message.getTo().equals(mChatRecord.getChateeName())) {
      textView.setGravity(Gravity.LEFT);
    } else {
      textView.setGravity(Gravity.RIGHT);
    }
    return view;
  }
}