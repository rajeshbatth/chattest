package com.rajesh.ChatTest.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.InjectView;
import butterknife.OnClick;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.adapter.ChatRecordAdapter;
import com.rajesh.ChatTest.chat.ChatClient;
import com.rajesh.ChatTest.dao.ChatMessage;
import com.rajesh.ChatTest.dao.ChatMessageDao;
import com.rajesh.ChatTest.dao.Chatter;
import com.rajesh.ChatTest.dao.Marshaller;
import com.rajesh.ChatTest.misc.Logger;
import java.util.List;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

import static com.rajesh.ChatTest.dao.ChatMessageDao.Properties.ChatterId;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class ChatActivity extends BaseActivity implements ChatManagerListener, MessageListener {

  private static final String EXTRA_CHATTER = "chatter";

  @InjectView(R.id.chat_msg)
  EditText mChatMessageEditText;

  @InjectView(R.id.chat_record)
  ListView mListView;

  private ChatRecordAdapter mChatRecordAdapter;

  private Chat mChat;

  private Chatter mChatter;
  private ChatMessageDao mChatMessageDao;
  private ChatClient mChatClient;

  public static void startChatActivity(Context context, Chatter chatter) {
    context.startActivity(getChatIntent(context, chatter));
  }

  public static Intent getChatIntent(Context context, Chatter chatter) {
    Intent intent = new Intent(context, ChatActivity.class);
    intent.putExtra(EXTRA_CHATTER, chatter);
    return intent;
  }

  public static void saveMessage(Message message, ChatMessageDao chatMessageDao, Chatter chatter) {
    ChatMessage chatMessage = Marshaller.marshallMessage(message);
    chatMessage.setChatterId(chatter.getId());
    boolean isTxedByMe = message.getTo().equals(chatter.getName());
    chatMessage.setIsTx(isTxedByMe);
    chatMessageDao.insert(chatMessage);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chat);
    Bundle extras = getIntent().getExtras();
    mChatter = extras.getParcelable(EXTRA_CHATTER);
    Log.d(LOG_TAG, "mChatter = " + mChatter.getName());
    bindChatService();
    if (mChatter.getNickName() != null) {
      setTitle(mChatter.getNickName());
    }
    Log.i(LOG_TAG, "mChatter.getId() = " + mChatter.getId());
    mChatMessageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        sendMessage();
        return true;
      }
    });
  }

  @Override
  public void onChatServiceConnected() {
    mChatRecordAdapter = new ChatRecordAdapter(this, mChatter.getId());
    mListView.setAdapter(mChatRecordAdapter);
    mListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
    initInBackground();
  }

  private void initInBackground() {
    new Thread() {
      @Override
      public void run() {
        init();
      }
    }.start();
  }

  private void init() {
    mChatClient = getChatClient();
    if (mChatter.getThread() != null) {
      mChat = mChatClient.openChat(mChatter.getThread());
    } else {
      mChat = mChatClient.createChat(mChatter.getName());
    }
    mChatMessageDao = getDaoSession().getChatMessageDao();
    mChat.addMessageListener(this);
    List<ChatMessage> chatMessages =
        mChatMessageDao.queryBuilder().where(ChatterId.eq(mChatter.getId())).build().list();
    mChatRecordAdapter.setChatMessages(chatMessages);
  }

  @Override
  public void onChatServiceDisconnected() {
    mChat.removeMessageListener(this);
    mChatMessageDao = null;
  }

  @OnClick(R.id.send_msg)
  void sendMessage() {
    String text = mChatMessageEditText.getText().toString();
    if (text.isEmpty()) {
      return;
    }
    try {
      Message message = new Message();
      message.setBody(text);
      message.setType(Message.Type.chat);
      mChat.sendMessage(message);
      mChatMessageEditText.setText("");
      saveMessage(message, mChatMessageDao, mChatter);
      mChatRecordAdapter.notifyDataSetChanged();
    } catch (XMPPException e) {
      e.printStackTrace();
      Logger.e("exception while sending message: " + e);
    }
  }

  @Override
  public void chatCreated(Chat chat, boolean b) {
    Logger.d("chatCreated " + b);
  }

  @Override
  public void processMessage(Chat chat, final Message message) {
    if (message.getType() == Message.Type.chat) {
      Log.d(LOG_TAG, "message = " + message.getFrom());
      mChatRecordAdapter.getChatMessages().add(Marshaller.marshallMessage(message));
      mChatRecordAdapter.postNotifyDataSetChanged();
    }
  }
}
