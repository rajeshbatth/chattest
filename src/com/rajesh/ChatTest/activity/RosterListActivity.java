package com.rajesh.ChatTest.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.rajesh.ChatTest.R;

/**
 * Created by Rajesh on 9/8/2014.
 */
public class RosterListActivity extends BaseActivity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_roster_list);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return false;
  }
}