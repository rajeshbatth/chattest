package com.rajesh.ChatTest.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.chat.ChatLoginHandler;
import com.rajesh.ChatTest.misc.Session;
import com.rajesh.ChatTest.model.AppUser;

public class LoginActivity extends BaseActivity {

  @InjectView(R.id.username)
  EditText mUsernameEditText;

  @InjectView(R.id.password)
  EditText mPasswordEditText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    bindChatService();
  }

  @Override
  public void onChatServiceConnected() {
    AppUser user = Session.getUser(this);
    if (user != null) {
      login(user);
    } else {
      setContentView(R.layout.activity_login);
    }
  }

  @OnClick(R.id.login_btn)
  void login() {
    AppUser appUser = new AppUser();
    appUser.setName(mUsernameEditText.getText().toString());
    appUser.setPassword(mPasswordEditText.getText().toString());
    login(appUser);
  }

  private void login(AppUser user) {
    getChatClient().loginAsync(new MyChatLoginHandler(user));
  }

  private void launchNextActivity() {
    finish();
    startActivity(new Intent(this, RosterListActivity.class));
  }

  private class MyChatLoginHandler extends ChatLoginHandler {

    private ProgressDialog mProgressDialog;

    public MyChatLoginHandler(AppUser appUser) {
      super(appUser);
    }

    @Override
    public void onStart() {
      mProgressDialog = ProgressDialog.show(LoginActivity.this, null, "Logging in..", false, false);
    }

    @Override
    public void onSuccess() {
      mProgressDialog.dismiss();
      Session.saveUser(LoginActivity.this, getAppUser());
      launchNextActivity();
    }

    @Override
    public void onFailure() {
      mProgressDialog.dismiss();
      Toast.makeText(LoginActivity.this, "Login failed!!", Toast.LENGTH_LONG).show();
    }
  }
}
