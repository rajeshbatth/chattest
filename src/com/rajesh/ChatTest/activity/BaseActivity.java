package com.rajesh.ChatTest.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import butterknife.ButterKnife;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.chat.BaseComponent;
import com.rajesh.ChatTest.chat.BaseComponentImpl;
import com.rajesh.ChatTest.chat.ChatClient;
import com.rajesh.ChatTest.dao.DaoSession;
import com.rajesh.ChatTest.misc.ActivityStateCheck;

/**
 * Created by Rajesh on 9/8/2014.
 */
public abstract class BaseActivity extends FragmentActivity implements BaseComponent {

  public static final String LOG_TAG = "CHAT_APP";
  private BaseComponent mBaseComponent;

  @Override
  public ChatClient getChatClient() {
    return mBaseComponent.getChatClient();
  }

  public DaoSession getDaoSession() {
    return mBaseComponent.getDaoSession();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mBaseComponent = new BaseComponentImpl(this, this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    ActivityStateCheck.getInstance().start();
  }

  @Override
  protected void onStop() {
    super.onStop();
    ActivityStateCheck.getInstance().stop();
    if (mBaseComponent.getChatClient() != null) {
      unbindChatService();
    }
  }

  @Override
  public void setContentView(int layoutResID) {
    super.setContentView(layoutResID);
    ButterKnife.inject(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.logout:
        logout();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void logout() {
    mBaseComponent.logout();
  }

  @Override
  public void bindChatService() {
    mBaseComponent.bindChatService();
  }

  @Override
  public void unbindChatService() {
    mBaseComponent.unbindChatService();
  }

  @Override
  public void onChatServiceConnected() {

  }

  @Override
  public void onChatServiceDisconnected() {

  }
}
