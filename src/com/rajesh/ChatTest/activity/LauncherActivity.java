package com.rajesh.ChatTest.activity;

import android.content.Intent;
import android.os.Bundle;
import com.rajesh.ChatTest.R;
import com.rajesh.ChatTest.service.ChatService;

/**
 * Created by Rajesh on 9/9/2014.
 */
public class LauncherActivity extends BaseActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_launcher);
    startService(new Intent(this, ChatService.class));
    bindChatService();
  }

  @Override
  public void onChatServiceConnected() {
    launchActivity();
  }

  private void launchActivity() {
    Intent intent = new Intent();
    if (!getChatClient().isLoggedIn()) {
      intent.setClass(this, LoginActivity.class);
    } else {
      intent.setClass(this, RosterListActivity.class);
    }
    finish();
    startActivity(intent);
  }
}