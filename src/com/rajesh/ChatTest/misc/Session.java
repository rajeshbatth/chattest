package com.rajesh.ChatTest.misc;

import android.content.Context;
import android.content.SharedPreferences;
import com.rajesh.ChatTest.model.AppUser;

/**
 * Created by Rajesh on 9/9/2014.
 */
public class Session {
  private static final String SESSION_PREF = "session_pref";
  private static final String APP_USER_PWD = "app_user_pwd";
  private static final String APP_USER_NAME = "app_user_name";

  private Session() {
  }

  public static void saveUser(Context context, AppUser appUser) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(SESSION_PREF, Context.MODE_PRIVATE);
    sharedPreferences.edit()
        .putString(APP_USER_NAME, appUser.getName())
        .putString(APP_USER_PWD, appUser.getPassword())
        .commit();
  }

  public static AppUser getUser(Context context) {
    SharedPreferences sharedPreferences =
        context.getSharedPreferences(SESSION_PREF, Context.MODE_PRIVATE);
    String name = sharedPreferences.getString(APP_USER_NAME, null);
    String pwd = sharedPreferences.getString(APP_USER_PWD, null);
    if (name == null || name.isEmpty() || pwd == null || pwd.isEmpty()) {
      return null;
    }
    AppUser appUser = new AppUser();
    appUser.setName(name);
    appUser.setPassword(pwd);
    return appUser;
  }

  public static void clear(Context context) {
    context.getSharedPreferences(SESSION_PREF, Context.MODE_PRIVATE).edit().clear().commit();
  }
}
