package com.rajesh.ChatTest.misc;

/**
 * Created by Rajesh on 9/14/2014.
 */
public class ActivityStateCheck {
  private static ActivityStateCheck ourInstance = new ActivityStateCheck();
  private int count = 0;

  private ActivityStateCheck() {
  }

  public static ActivityStateCheck getInstance() {
    return ourInstance;
  }

  public void start() {
    count++;
  }

  public void stop() {
    count--;
  }

  public boolean isActive() {
    return count > 0;
  }
}
