package com.rajesh.ChatTest.misc;

import android.util.Log;
import com.rajesh.ChatTest.BuildConfig;

/**
 * Created by Rajesh on 9/10/2014.
 */
public class Logger {
  private static boolean mEnabled = BuildConfig.DEBUG;
  private static String mTag = "CHAT_APP";

  public static int d(String msg) {
    if (mEnabled) {
      return Log.d(mTag, msg);
    }
    return 0;
  }

  public static int e(String msg) {
    if (mEnabled) {
      return Log.e(mTag, msg);
    }
    return 0;
  }

  public static int e(String msg, Throwable throwable) {
    if (mEnabled) {
      return Log.e(mTag, msg, throwable);
    }
    return 0;
  }

  public static int wtf(String msg) {
    if (mEnabled) {
      return Log.wtf(mTag, msg);
    }
    return 0;
  }
}
