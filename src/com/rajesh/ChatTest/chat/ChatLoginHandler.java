package com.rajesh.ChatTest.chat;

import android.os.Handler;
import android.os.Message;
import com.rajesh.ChatTest.model.AppUser;

/**
 * Created by Rajesh on 9/10/2014.
 */
public abstract class ChatLoginHandler extends Handler {
  public static final int STARTED = 1;
  public static final int SUCCESS = 2;
  public static final int FAILED = 3;
  private final AppUser mAppUser;

  public ChatLoginHandler(AppUser appUser) {
    mAppUser = appUser;
  }

  @Override
  public void handleMessage(Message msg) {
    switch (msg.what) {
      case STARTED:
        onStart();
        break;
      case SUCCESS:
        onSuccess();
        break;
      case FAILED:
        onFailure();
        break;
    }
  }

  public abstract void onStart();

  public abstract void onSuccess();

  public abstract void onFailure();

  public AppUser getAppUser() {
    return mAppUser;
  }
}
