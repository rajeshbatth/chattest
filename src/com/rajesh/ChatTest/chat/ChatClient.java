package com.rajesh.ChatTest.chat;

import android.os.Binder;
import android.os.Message;
import com.rajesh.ChatTest.conf.Constants;
import com.rajesh.ChatTest.model.AppUser;
import java.io.IOException;
import java.util.Collection;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;

/**
 * Created by Rajesh on 9/9/2014.
 */
public class ChatClient extends Binder implements MessageListener {

  private XMPPConnection mXmppConnection;
  private MessageListener mMessageListener;
  private ChatManager mChatManager;
  private ChatManagerListener mBackgroundChatListener;

  public ChatClient() {
    ConnectionConfiguration configuration =
        new ConnectionConfiguration(Constants.SERVER_URL, Constants.SERVER_PORT);
    configuration.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
    mXmppConnection = new XMPPConnection(configuration);
  }

  public void login(AppUser user) throws IOException, XMPPException {
    mXmppConnection.connect();
    mChatManager = mXmppConnection.getChatManager();
    mXmppConnection.login(user.getName(), user.getPassword());
    setPresence(Presence.Type.available);
    if (mBackgroundChatListener != null) {
      setBackgroundChatListener(mBackgroundChatListener);
    }
  }

  public void setPresence(Presence.Type available) {
    Presence presence = new Presence(available);
    mXmppConnection.sendPacket(presence);
  }

  public void loginAsync(final ChatLoginHandler handler) {
    new Thread(new Runnable() {
      @Override
      public void run() {
        Message msg = new Message();
        try {
          login(handler.getAppUser());
          msg.what = ChatLoginHandler.SUCCESS;
        } catch (Exception e) {
          msg.what = ChatLoginHandler.FAILED;
        }
        handler.sendMessage(msg);
      }
    }).start();
    handler.onStart();
  }

  public boolean isLoggedIn() {
    return mXmppConnection != null && mXmppConnection.isAuthenticated();
  }

  public void logout() {
    mXmppConnection.disconnect();
  }

  public void setBackgroundChatListener(ChatManagerListener listener) {
    if (mChatManager != null) {
      if (mBackgroundChatListener != null) {
        mChatManager.removeChatListener(mBackgroundChatListener);
      }
      mChatManager.addChatListener(listener);
    }
    mBackgroundChatListener = listener;
  }

  public void addChatListener(ChatManagerListener listener) {
    mChatManager.addChatListener(listener);
  }

  public void removeChatListener(ChatManagerListener listener) {
    mChatManager.removeChatListener(listener);
  }

  public Collection<RosterEntry> getRosterEntries() {
    return mXmppConnection.getRoster().getEntries();
  }

  public Chat createChat(String chateeName) {
    return mChatManager.createChat(chateeName, this);
  }

  public Chat openChat(String thread) {
    Chat chat = mChatManager.getThreadChat(thread);
    if (chat != null) {
      chat.addMessageListener(this);
    }
    return chat;
  }

  public void destroy() {
    if (mXmppConnection.isConnected()) {
      mXmppConnection.disconnect();
    }
    mXmppConnection = null;
    mChatManager = null;
    mBackgroundChatListener = null;
    mMessageListener = null;
  }

  @Override
  public void processMessage(Chat chat, org.jivesoftware.smack.packet.Message message) {
    if (mMessageListener != null) {
      mMessageListener.processMessage(chat, message);
    }
  }
}
