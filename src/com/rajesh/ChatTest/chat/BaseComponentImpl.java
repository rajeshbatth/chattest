package com.rajesh.ChatTest.chat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.MenuItem;
import com.rajesh.ChatTest.ChatApp;
import com.rajesh.ChatTest.activity.BaseActivity;
import com.rajesh.ChatTest.activity.LoginActivity;
import com.rajesh.ChatTest.dao.DaoSession;
import com.rajesh.ChatTest.misc.Session;
import com.rajesh.ChatTest.service.ChatService;

/**
 * Created by Rajesh on 9/14/2014.
 */
public class BaseComponentImpl implements BaseComponent, ServiceConnection {

  private final ChatServiceBinder mServiceBinder;
  private final Context mContext;
  private final DaoSession mDaoSession;
  private ChatClient mChatClient;

  public BaseComponentImpl(BaseActivity context, ChatServiceBinder serviceBinder) {
    mContext = context;
    mServiceBinder = serviceBinder;
    mDaoSession = ((ChatApp) mContext.getApplicationContext()).getDaoSession();
  }

  @Override
  public DaoSession getDaoSession() {
    return mDaoSession;
  }

  @Override
  public void logout() {
    mChatClient.logout();
    Session.clear(mContext);
    Intent intent = new Intent(mContext, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
    mContext.startActivity(intent);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return false;
  }

  @Override
  public void bindChatService() {
    mContext.bindService(new Intent(mContext, ChatService.class), this, Context.BIND_AUTO_CREATE);
  }

  @Override
  public void unbindChatService() {
    mContext.unbindService(this);
    mChatClient = null;
  }

  @Override
  public void onChatServiceConnected() {
    mServiceBinder.onChatServiceConnected();
  }

  @Override
  public void onChatServiceDisconnected() {
    mServiceBinder.onChatServiceDisconnected();
  }

  @Override
  public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
    mChatClient = (ChatClient) iBinder;
    onChatServiceConnected();
  }

  @Override
  public void onServiceDisconnected(ComponentName componentName) {
    mChatClient = null;
    onChatServiceDisconnected();
  }

  @Override
  public ChatClient getChatClient() {
    return mChatClient;
  }
}
