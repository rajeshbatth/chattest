package com.rajesh.ChatTest.chat;

import android.view.MenuItem;
import com.rajesh.ChatTest.dao.DaoSession;

/**
 * Created by Rajesh on 9/14/2014.
 */
public interface BaseComponent extends ChatServiceBinder {
  public DaoSession getDaoSession();

  public void logout();

  public boolean onOptionsItemSelected(MenuItem item);
}
