package com.rajesh.ChatTest.chat;

/**
 * Created by Rajesh on 9/10/2014.
 */
public interface ChatServiceBinder {

  public void bindChatService();

  public void unbindChatService();

  public void onChatServiceConnected();

  public void onChatServiceDisconnected();

  public ChatClient getChatClient();
}
