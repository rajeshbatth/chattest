package com.rajesh.ChatTest.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.rajesh.ChatTest.service.ChatService;

/**
 * Created by Rajesh on 9/10/2014.
 */
public class BootUpReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
    ChatService.onBootUp(context);
  }
}
