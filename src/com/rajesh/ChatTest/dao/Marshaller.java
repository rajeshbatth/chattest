package com.rajesh.ChatTest.dao;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.packet.Message;

/**
 * Created by Rajesh on 9/13/2014.
 */
public class Marshaller {
  public static Chatter marshallChatter(RosterEntry rosterEntry) {
    Chatter chatter = new Chatter();
    chatter.setName(rosterEntry.getUser());
    chatter.setNickName(rosterEntry.getName());
    return chatter;
  }

  public static ChatMessage marshallMessage(Message message) {
    ChatMessage chatMessage = new ChatMessage();
    chatMessage.setBody(message.getBody());
    return chatMessage;
  }

  public static Message unmarshallMessage(ChatMessage chatMessage) {
    Message message = new Message();
    message.setBody(chatMessage.getBody());
    return message;
  }
}
