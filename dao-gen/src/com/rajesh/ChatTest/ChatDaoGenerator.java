package com.rajesh.ChatTest;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import java.io.IOException;

/**
 * Created by Rajesh on 9/12/2014.
 */
public class ChatDaoGenerator {
  public static void main(String[] args) {
    Schema schema = new Schema(1, "com.rajesh.ChatTest.dao");
    schema.enableKeepSectionsByDefault();

    Entity message = schema.addEntity("ChatMessage");
    message.implementsInterface("Parcelable");
    message.addIdProperty();
    message.addStringProperty("body");
    message.addDateProperty("sentTime");
    message.addDateProperty("deliveredTime");
    message.addIntProperty("status");
    message.addStringProperty("thread");
    message.addBooleanProperty("isSeen");
    message.addBooleanProperty("isTx");

    Entity chatter = schema.addEntity("Chatter");
    chatter.addIdProperty();
    chatter.implementsInterface("Parcelable");
    chatter.addStringProperty("name").unique();
    chatter.addStringProperty("nickName");
    chatter.addStringProperty("thread");

    Property chatterId = message.addLongProperty("chatterId").getProperty();
    chatter.addToMany(message, chatterId);

    try {
      DaoGenerator daoGenerator = new DaoGenerator();
      daoGenerator.generateAll(schema, "src");
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
